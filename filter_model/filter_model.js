import { LightningElement, track,api } from 'lwc';

export default class Iowa_filter_model extends LightningElement {
    @api options;
    @track filterValue;
    @track  filterSelect;
    header = 'add filter';

    handleFilterSelect(event) {
        this.filterSelect = event.detail.value;
    }

    handleFilterValue(event) {
        this.filterValue = event.detail.value;
    }

    handleCloseModal() {
		// Creates the event with the data.
		const selectedEvent = new CustomEvent("closemodal", {
			detail: ''
		});
		// Dispatches the event.
		this.dispatchEvent(selectedEvent);
	}

    handleSave() {
		// Creates the event with the data.
		const selectedEvent = new CustomEvent("closemodal", {
			detail: { filterValue : this.filterValue,filterSelect:this.filterSelect}
		});
		// Dispatches the event.
		this.dispatchEvent(selectedEvent);
	}
}