import { LightningElement,track, api, wire } from 'lwc';

export default class Iowa_dashboard_filter extends LightningElement {
  
    @track openFilterModel= false;
    @api filterMap=[];
    get options() {
        return [
            { label: 'Company', value: 'Company' },
            { label: 'Type', value: 'Type' },
            { label: 'Company City', value: 'Company City' },
        ];
    }
    handleOpenFilterAction(event) {
		this.openFilterModel = true;
	}

    handleCloseFilterAction(event) {
		this.openFilterModel = false;
        let filterSelect = event.detail.filterSelect;
        let filterValue = event.detail.filterValue;
        if(filterSelect && filterValue){
        this.filterMap.push({name:filterSelect,value:filterValue,labelValue:filterSelect+' is '+filterValue});
        }
        // Creates the event with the data.
		const selectedEvent = new CustomEvent("filterchange", {
			detail: { filterMap : this.filterMap}
		});
		// Dispatches the event.
		this.dispatchEvent(selectedEvent);     
	}
    handleRemoveOnly(event) {
        console.log('event'+event.detail.name);
        let currentValue=event.detail.name;
        var _filterMap=this.filterMap;
        for(var i=0;i<_filterMap.length;i++){
            console.log('this.filterMap[i].value '+_filterMap[i].value );
            if(_filterMap[i].value == currentValue){
                console.log(' test ');
                this.filterMap.splice(this.filterMap.indexOf(_filterMap[i].value), 1);
            }
        }
        console.log('ttesde'+JSON.stringify(this.filterMap));
        // Creates the event with the data.
		const selectedEvent = new CustomEvent("filterchange", {
			detail: { filterMap : this.filterMap}
		});
		// Dispatches the event.
		this.dispatchEvent(selectedEvent);     
    }

}